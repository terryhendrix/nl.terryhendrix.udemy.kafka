#!/bin/bash

if [[ -z $1 ]]; then
  CMD=bash
else
  CMD=$@
fi

docker run --rm -it --net=host landoop/fast-data-dev $CMD
