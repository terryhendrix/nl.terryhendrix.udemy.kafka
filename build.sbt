import sbt._

name := "kafka.beginner.to.intermediate"

organization := "nl.terryhendrix.udemy"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.apache.kafka"  % "kafka-clients"   % "0.10.2.0",
  "com.typesafe.akka" % "akka-actor_2.11" % "2.5.0",
  "com.typesafe.akka" % "akka-slf4j_2.11" % "2.5.0",
  "ch.qos.logback"    % "logback-classic" % "1.1.3"
)