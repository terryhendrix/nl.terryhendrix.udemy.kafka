#!/bin/bash

# Creates a topic with the given name
# NOTE: replication 1 is only for development purposes, in production you want to have 2 or even 3.
function createTopic {
    ./kafka-cli.sh kafka-topics --zookeeper=127.0.0.1:2181 --create --topic $1 --partition 3 --replication 1
}

function describeTopic {
    ./kafka-cli.sh kafka-topics --zookeeper=127.0.0.1:2181 --describe --topic $1
}


function listTopics {
    ./kafka-cli.sh kafka-topics --zookeeper=127.0.0.1:2181 --list
}

# Start kafka
export COMPOSE_PROJECT_NAME=example
docker-compose up -d

# Configure topics
sleep 3
createTopic RegistrationCompleted
describeTopic RegistrationCompleted
createTopic EmailVerified
describeTopic EmailVerified
listTopics
