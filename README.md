# Apache Kafka Udemy Course

This is a study project for the Udemy course [Apache Kafka Series - Learning Apache Kafka for Beginners](https://www.udemy.com/apache-kafka-series-kafka-from-beginner-to-intermediate/).

## Project setup

In this project 3 application components exist, which communicate by Kafka.

1. A registration component, it handles the registration of new customers in our system.
2. An email verification component, which sends emails for newly registered customers. The customers may then use the email to verify it's email address.
3. A customer component, which stores newly created customers. A customer object is created after a succesful registration.

In a micro service architecture, these components should be devided over services. However, for the simplicity of the study project, they all reside in a single service.

### Kafka setup

For this study project, 1 Kafka instance runs in a docker. Replication factor N=3 should used, however for sake of the study project N=1 is used.
At least once message delivery is used, this means data won't be lost in case of failure (opposed to at most once delivery). Kafka does this by committing the offset AFTER the message is processed, instead of BEFORE processing (at most once delivery).

If you're on native docker, use `start-cluster.sh` to start the kafka cluster.
If you use docker toolbox, go to the `docker-compose.yml` file and change the ADV_HOST to `192.168.99.100`.
The kafka cluster may be stopped with `stop-cluster.sh`

After starting the docker containers, go to [http://127.0.0.1:3030](http://127.0.0.1:3030) to view the UI.

#### Kafka CLI

Now you may connect to the kafka docker using the `kafka-cli.sh` script. Once you get a terminal you may use the following commands:

List topics:

`kafka-topics --zookeeper 127.0.0.1 --list`

Create a topic. In production, the replication factor should be 2 or 3, the start script uses 1.

`kafka-topics --zookeeper=127.0.0.1:2181 --create --topic <NAME> --partition <N> --replication <N>`

Produce to a topic, type messages after issueing the following command:

`kafka-console-producer --broker-list 127.0.0.1:9092 --topic <NAME>`

Consume from a topic, starts at the end of the topic:

`kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic RegistrationCompleted`

Consume from topic from beginning:

`kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic RegistrationCompleted --from-beginning`

Consume from a single partition within a topic from beginning:

`kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic RegistrationCompleted --from-beginning --partition 1`

Consume from a topic with a consumer group, it commits the offset. If command is executed more than once, there will be no message duplicates.

`kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic RegistrationCompleted --from-beginning --consumer-property group.id=marketing`

#### Write acknowledgements

Kafka has 3 modes for write consistency. These are:

1. acks=0, no acknowledgement, most high performance but greater change to data loss.
2. acks=1, leader acknowledgement.
3. acks=all, leader + replica's acknowledgement, least performance but also no data loss during message publishing.

## Persistence

There is no persistence in this project, other than kafka. So no database is used, everything runs in RAM.


