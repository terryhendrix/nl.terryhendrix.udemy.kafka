package nl.terryhendrix.kafka

import java.util
import java.util.Properties

import akka.event.LoggingAdapter

import scala.collection.JavaConversions._
import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}

import scala.annotation.tailrec
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Created by terryhendrix on 30/04/2017.
  */
object Kafka {
  object Topics extends Enumeration {
    val RegistrationCompleted, EmailVerified = Value
  }

  private object KafkaProperties {
    def consumer(groupId:String, autoCommit:Boolean = true, autoCommitInterval:Int = 1000) = {
      val p = new Properties()
      p.setProperty("bootstrap.servers", "127.0.0.1:9092")
      p.setProperty("key.deserializer", classOf[StringDeserializer].getName)
      p.setProperty("value.deserializer", classOf[StringDeserializer].getName)

      p.setProperty("group.id", groupId)
      p.setProperty("enable.auto.commit", autoCommit.toString)

      if(autoCommit)
        p.setProperty("auto.commit.interval.ms", autoCommitInterval.toString)

      /*
      * What to do when there is no initial offset in Kafka or if the current offset does not exist any more on the server
      * (e.g. because that data has been deleted) */
      p.setProperty("auto.offset.reset", "earliest")

      p
    }


    def producer() = {
      val p = new Properties()
      p.setProperty("bootstrap.servers", "127.0.0.1:9092")
      p.setProperty("key.serializer", classOf[StringSerializer].getName)
      p.setProperty("value.serializer", classOf[StringSerializer].getName)

      p.setProperty("acks", "1")               // 0 = no acks, 1 = leader acks, all = leader + replica acks
      p.setProperty("retries", "3")

      p
    }
  }

  def producer(topic:Topics.Value)(implicit log:LoggingAdapter) = {
    new Producer(topic.toString)
  }

  def consumer(topic:Topics.Value, groupId:String, autoAck:Boolean = true)(implicit log:LoggingAdapter) = {
    new Consumer(topic.toString, groupId, autoAck)
  }

  class Consumer(topic:String, groupId:String, autoAck:Boolean)(implicit log:LoggingAdapter) {
    log.info(s"Starting consumer, topic=$topic, groupId=$groupId, autoAck=$autoAck.")
    private val consumer = new KafkaConsumer[String,String](KafkaProperties.consumer(groupId))

    consumer.subscribe(util.Arrays.asList(topic))

    def consumeWith(f:ConsumerRecord[String,String] => Unit)(implicit ec:ExecutionContext): Unit = {
      Future {
        val records = consumer.poll(1000)

        records.foreach { record =>
          log.debug(s"$topic($groupId) consumed ${record.value()}")
          f(record)
        }

        if(!autoAck)
          consumer.commitSync()
      }.recover { case ex =>
        log.error(s"Error while consuming: ${ex.getMessage}")
      }.map { _ =>
        consumeWith(f)
      }
    }

    def close() = consumer.close()
  }

  class Producer(topic:String)(implicit log:LoggingAdapter) {
    log.info(s"Starting producer, topic=$topic.")
    private val producer = new KafkaProducer[String,String](KafkaProperties.producer())

    def produce(message:String) = {
      val record = new ProducerRecord[String, String](topic, message.hashCode.toString, message)
      producer.send(record)
      producer.flush()
      log.debug(s"$topic produced ${record.value()}")
    }

    def close() = producer.close()
  }
}
