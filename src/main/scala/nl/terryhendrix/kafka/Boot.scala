package nl.terryhendrix.kafka

import akka.actor.{ActorSystem, Props}
import nl.terryhendrix.kafka.components.{Customers, EmailVerification, Registration}

/**
  * Created by terryhendrix on 30/04/2017.
  */
object Boot extends App {
  val system = ActorSystem("KafkaExample")
  val registration = new Registration(system)
  val emailVerification = system.actorOf(Props(new EmailVerification()), "EmailVerification")
  val customers = system.actorOf(Props(new Customers()), "Customers")

  private val customer1 = "Terry Hendrix"
  private val customer2 = "Babs Strijkert"
  private val customer3 = "Erik Poldervaart"
  private val customer4 = "Marnix Indenbosch"

  registration.completeRegistration(customer3)
  registration.completeRegistration(customer1)
  registration.completeRegistration(customer2)
  registration.completeRegistration(customer4)
}


