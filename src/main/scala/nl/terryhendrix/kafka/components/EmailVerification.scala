package nl.terryhendrix.kafka.components

import java.util.UUID

import akka.actor.{Actor, ActorLogging}
import nl.terryhendrix.kafka.Kafka
import nl.terryhendrix.kafka.components.EmailVerification.VerifyEmail

object EmailVerification {
  private val consumerGroup = "EmailVerification"

  sealed case class Verification(customer:String, verificationCode:String)
  sealed case class RegistrationCompleted(customer:String)
  sealed case class EmailVerified(customer:String)
  sealed case class VerifyEmail(verificationCode:String)
}

class EmailVerification extends Actor with ActorLogging
{
  import context.dispatcher
  var pendingVerifications = Seq.empty[EmailVerification.Verification]

  val producer = Kafka.producer(Kafka.Topics.EmailVerified)(log)
  val consumer = Kafka.consumer(Kafka.Topics.RegistrationCompleted, EmailVerification.consumerGroup, autoAck = false)(log)

  consumer.consumeWith { record =>
    self ! EmailVerification.RegistrationCompleted(record.value())
  }

  override def preStart() = {
    log.info("Starting EmailVerification.")
  }

  override val receive: Receive = {
    case event:EmailVerification.RegistrationCompleted if pendingVerifications.exists(_.customer == event.customer) =>
      log.info(s"Email verification for ${event.customer} already exists.")
      logState()

    case event:EmailVerification.RegistrationCompleted =>
      log.info(s"Sending verification email for ${event.customer}.")
      val verification = EmailVerification.Verification(event.customer, verificationCode = UUID.randomUUID().toString)
      pendingVerifications = pendingVerifications :+ verification
      self ! VerifyEmail(verification.verificationCode)  // mimick that the email is verified, normally this would come from an external source over (maybe) a rest API?
      logState()

    case command:EmailVerification.VerifyEmail if pendingVerifications.exists(_.verificationCode == command.verificationCode) =>
      pendingVerifications.find(_.verificationCode == command.verificationCode).foreach { verification =>
        log.info(s"${verification.customer} clicked on the email verification link sent to in the email.")
        producer.produce(verification.customer)
      }

    case command:EmailVerification.VerifyEmail =>
      log.info(s"Could not verify email, unknown verification code ${command.verificationCode}.")
  }

  def logState() = {
    log.info(s"Tracking ${pendingVerifications.size} email verifications.")
  }

  override def unhandled(message: Any): Unit = log.warning(s"Unhandled $message")
}
