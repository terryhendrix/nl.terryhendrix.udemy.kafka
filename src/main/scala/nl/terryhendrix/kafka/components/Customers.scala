package nl.terryhendrix.kafka.components

import akka.actor.{Actor, ActorLogging}
import nl.terryhendrix.kafka.Kafka

object Customers {
  private val consumerGroup = "Customers"

  sealed case class RegistrationCompleted(customer:String)
  sealed case class EmailVerified(customer:String)
  sealed case class Customer(name:String, active:Boolean)
}

class Customers extends Actor with ActorLogging {
  import context.dispatcher
  var customers:Seq[Customers.Customer] = Seq.empty

  val registrationCompleted = Kafka.consumer(Kafka.Topics.RegistrationCompleted, Customers.consumerGroup, autoAck = false)(log)
  val emailVerified = Kafka.consumer(Kafka.Topics.EmailVerified, Customers.consumerGroup, autoAck = false)(log)

  registrationCompleted.consumeWith { record =>
    self ! Customers.RegistrationCompleted(record.value())
  }

  emailVerified.consumeWith { record =>
    self ! Customers.EmailVerified(record.value())
  }

  override def preStart() = {
    log.info("Starting Customers.")
  }

  override val receive: Receive = {
    case event:Customers.RegistrationCompleted if customers.exists(_.name == event.customer) =>
      log.info(s"Customer ${event.customer} already exists.")
      logState()

    case event:Customers.RegistrationCompleted =>
      log.info(s"Creating un-activated customer ${event.customer}.")
      customers = customers :+ Customers.Customer(event.customer, active = false)

    case event:Customers.EmailVerified if customers.exists(c => c.name == event.customer && !c.active) =>
      customers = customers.map {
        case customer if customer.name == event.customer =>
          log.info(s"Activating customer ${customer.name}.")
          customer.copy(active = true)
        case c => c
      }

    case event:Customers.EmailVerified if customers.exists(c => c.name == event.customer && c.active) =>
      log.info(s"Customer ${event.customer} is already active.")
      logState()

    case event:Customers.EmailVerified =>
      log.info("It appears that the EmailVerified arrived before RegistrationCompleted, compensating for this by adding an activated customer.")
      customers = customers :+ Customers.Customer(event.customer, active = true)
  }

  def logState() = {
    log.info(s"Got ${customers.count(_.active)} activated and ${customers.count(!_.active)} un-activated customers.")
  }

  override def unhandled(message: Any): Unit = log.warning(s"Unhandled $message")
}
