package nl.terryhendrix.kafka.components

import akka.actor.{ActorSelection, ActorSystem}
import akka.event.Logging
import nl.terryhendrix.kafka.Kafka

class Registration(system:ActorSystem)
{
  val log = Logging(system, "Registration")
  val producer = Kafka.producer(Kafka.Topics.RegistrationCompleted)(log)

  def completeRegistration(customerName:String) = {
    producer.produce(customerName)
  }
}


